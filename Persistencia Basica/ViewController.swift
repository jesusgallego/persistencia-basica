//
//  ViewController.swift
//  Persistencia Basica
//
//  Created by Master Móviles on 21/12/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextViewDelegate {
    
    var fechaEdicion: Date!
    
    private let filename = "mensaje.plist"

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.textView.delegate = self
        
        
        // Get Default data
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        var dict : NSDictionary?
        if urls.count > 0 {
            let docDir = urls[0]
            let urlPlist = docDir.appendingPathComponent(self.filename)
            
            dict = NSDictionary(contentsOf: urlPlist)
            
            if let unwrappedDict = dict {
                if let msg = unwrappedDict["message"], let date = unwrappedDict["date"] {
                    textView.text = msg as! String
                    self.fechaEdicion = date as! Date
                    label.text = DateFormatter.localizedString(from: self.fechaEdicion, dateStyle: .short, timeStyle: .short)
                }
            }
        }
        
        
        // Action when app goes to Background
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(self.toBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.textView.resignFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.fechaEdicion = Date()
        
        self.label.text = DateFormatter.localizedString(from: self.fechaEdicion, dateStyle: .short, timeStyle: .short)
    }
    
    func toBackground() {
        print("App goes to background")
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        if urls.count > 0 {
            let docDir = urls[0]
            
            let urlPlist = docDir.appendingPathComponent(self.filename)
            
            let data : [String: Any] = [
                "message" : textView.text,
                "date" : self.fechaEdicion
            ]
            
            let dict = data as NSDictionary
            
            dict.write(to: urlPlist, atomically: true)
        }
    }
}

